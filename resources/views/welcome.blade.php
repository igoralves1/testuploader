<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title -->
    <title>Unify Forms | Unify - Responsive Website Template</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="../../../favicon.ico">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/bootstrap.min.css') }}">
    <!-- CSS Global Icons -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/icon-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/icon-line/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/icon-etlinefont/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/icon-line-pro/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/icon-hs/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/themify-icons/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/dzsparallaxer/dzsparallaxer.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/dzsparallaxer/dzsscroller/scroller.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/dzsparallaxer/advancedscroller/plugin.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/jquery-ui/themes/base/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/hs-megamenu/src/hs.megamenu.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/hamburgers/hamburgers.min.css') }}">

    <!-- Show / Copy Code -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/prism/themes/prism.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/custombox/custombox.min.css') }}">


    <!-- CSS Unify -->
    <link rel="stylesheet" href="{{ asset('assets/css/unify-core.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/unify-components.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/unify-globals.css') }}">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
</head>

<body class="u-body--header-side-push-left u-body--header-side-opened u-has-sidebar-navigation g-overflow-x-hidden">
<main>



    <!-- Form Controls -->
    <section class="container g-py-100">

        <!-- File Inputs -->
        <div class="row">
            <div class="col-md-2">
                <h3 class="h4 g-font-weight-300">File Inputs
                    <small class="text-muted">(attachments)</small>
                </h3>
            </div>

            <div class="col-md-10">
                <div id="shortcode3">
                    <div class="shortcode-html">
                        <!-- File Inputs -->
                        <form class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30">
                            <!-- Advanced File Input -->
                            <div class="form-group mb-0">
                                <label class="g-mb-10">Advanced File input</label>
                                <input class="js-file-attachment" type="file" name="fileAttachment2[]">
                            </div>
                            <!-- End Advanced File Input -->
                        </form>
                        <!-- End File Inputs -->
                    </div>

                    <div class="shortcode-scripts">
                        <!-- JS Implementing Plugins -->
                        <script type="text/plain" src="../../../assets/vendor/jquery.filer/js/jquery.filer.min.js"></script>

                        <!-- JS Unify -->
                        <script type="text/plain" src="../../../assets/js/helpers/hs.focus-state.js"></script>
                        <script type="text/plain" src="../../../assets/js/components/hs.file-attachement.js"></script>

                        <!-- JS Plugins Init. -->
                        <script type="text/plain">
                $(document).on('ready', function () {
                      // initialization of forms
                      $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
                      $.HSCore.helpers.HSFocusState.init();
                    });
              </script>
                    </div>
                </div>



                <!-- Show Code -->
                <div class="g-font-size-12 g-my-30 ">
                    <a class="js-modal-markup u-link-v5  g-color-main  g-color-primary--hover g-mr-15" href="#!" data-content-target="#shortcode3" data-modal-target="#modalMarkup" data-modal-effect="fadein">
                        <i class="fa fa-code"></i>
                        Show code
                    </a>
                    <a class="js-copy u-link-v5  g-color-main  g-color-primary--hover" href="#!" data-content-target="#shortcode3" data-success-text="Copied">
                        <i class="fa fa-clone"></i>
                        Copy code
                    </a>
                </div>
                <!-- End Show Code -->

            </div>
        </div>
        <!-- End File Inputs -->

    </section>
    <!-- End Form Controls -->

    <hr class="g-brd-gray-light-v4 my-0">


    <a class="js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{
     "bottom": 15,
     "right": 15
   }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
        <i class="hs-icon hs-icon-arrow-top"></i>
    </a>
</main>

<div id="modalMarkup" class="text-left modal-demo g-width-95x g-height-95x g-bg-white g-color-black g-pa-20" style="display: none;"></div>


<!-- JS Global Compulsory -->
<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-migrate/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('assets/vendor/popper.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js') }}"></script>


<!-- JS Implementing Plugins -->
<script src="{{ asset('assets/vendor/hs-megamenu/src/hs.megamenu.js') }}"></script>
<script src="{{ asset('assets/vendor/dzsparallaxer/dzsparallaxer.js') }}"></script>
<script src="{{ asset('assets/vendor/dzsparallaxer/dzsscroller/scroller.js') }}"></script>
<script src="{{ asset('assets/vendor/dzsparallaxer/advancedscroller/plugin.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery.maskedinput/src/jquery.maskedinput.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery.filer/js/jquery.filer.min.js') }}"></script>

<!-- jQuery UI Core -->
<script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.core.min.js') }}"></script>


<!-- jQuery UI Helpers -->
<script src="{{ asset('assets/vendor/jquery-ui/ui/widgets/menu.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-ui/ui/widgets/mouse.js') }}"></script>

<!-- jQuery UI Widgets -->
<script src="{{ asset('assets/vendor/jquery-ui/ui/widgets/autocomplete.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-ui/ui/widgets/datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-ui/ui/widgets/slider.js') }}"></script>

<!-- JS Unify -->
<script src="{{ asset('assets/js/hs.core.js') }}"></script>

<script src="{{ asset('assets/js/components/hs.header.js') }}"></script>
<script src="{{ asset('assets/js/helpers/hs.hamburgers.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.header-side.js') }}"></script>

<script src="{{ asset('assets/js/helpers/hs.rating.js') }}"></script>
<script src="{{ asset('assets/js/helpers/hs.not-empty-state.js') }}"></script>
<script src="{{ asset('assets/js/helpers/hs.focus-state.js') }}"></script>
<script src="{{ asset('assets/js/helpers/hs.file-attachments.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.file-attachement.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.datepicker-1.0.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.slider.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.masked-input.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.count-qty.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.autocomplete.js') }}"></script>

<script src="{{ asset('assets/js/components/hs.autocomplete-local-search.js') }}"></script>
<script src="{{ asset('assets/vendor/cookiejs/jquery.cookie.js') }}"></script>
<script src="{{ asset('assets/js/helpers/hs.shortcode-filter.js') }}"></script>

<script src="{{ asset('assets/js/components/hs.go-to.js') }}"></script>

<!-- Show / Copy Code -->
<script src="{{ asset('assets/vendor/clipboard/dist/clipboard.min.js') }}"></script>
<script src="{{ asset('assets/vendor/prism/prism.core.min.js') }}"></script>

<script src="{{ asset('assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('assets/vendor/custombox/custombox.min.js') }}"></script>

<script src="{{ asset('assets/js/components/hs.scrollbar.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.modal-window.js') }}"></script>
<script src="{{ asset('assets/js/helpers/hs.modal-markup.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.markup-copy.js') }}"></script>
<script src="{{ asset('assets/js/components/hs.tabs.js') }}"></script>

<script>
    $(document).on('ready', function () {
        $.HSCore.helpers.HSModalMarkup.init('.js-modal-markup');

        $.HSCore.components.HSMarkupCopy.init('.js-copy');
    });
</script>


<!-- JS Custom -->
<script src="{{ asset('assets/js/custom.js') }}"></script>

<!-- JS Plugins Init. -->
<script>
    $(document).on('ready', function () {alert('resources/views/welcome.blade.php - load 2 - l214 ');
        // initialization of forms
        $.HSCore.helpers.HSFileAttachments.init();
        $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
        $.HSCore.helpers.HSRating.init();
        $.HSCore.helpers.HSFocusState.init();
        $.HSCore.helpers.HSNotEmptyState.init();
        $.HSCore.components.HSDatepicker.init('#datepickerDefault, #datepickerInline, #datepickerInlineFrom, #datepickerFrom');
        $.HSCore.components.HSSlider.init('#regularSlider, #regularSlider2, #regularSlider3, #rangeSlider, #rangeSlider2, #rangeSlider3, #stepSlider, #stepSlider2, #stepSlider3');
        $.HSCore.components.HSMaskedInput.init('[data-mask]');
        $.HSCore.components.HSCountQty.init('.js-quantity');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');
    });

    $(window).on('load', function () {
        // initialization of autocomplet
        $.HSCore.components.HSLocalSearchAutocomplete.init('#u-sidebar-navigation__search-autocomplete');

        // initialization of autocomplet
        $.HSCore.components.HSAutocomplete.init('#autocomplete2');

        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');
        $.HSCore.components.HSHeaderSide.init($('#sideNav'));

        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });
    });
</script>
</body>

</html>
