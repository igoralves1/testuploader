### Laravel AJAX File Upload

#### Laravel 5.7 & Ajax Crud tutorial series || Laravel - Ajax

- https://www.youtube.com/playlist?list=PLbC4KRSNcMnrfKOVpsxp8JfhF0lTOlj5z
- https://www.youtube.com/watch?v=DMvBe4X1XDc&index=4&list=PLbC4KRSNcMnrfKOVpsxp8JfhF0lTOlj5z
- https://www.youtube.com/watch?v=KkUG3nfcsIM

- https://appdividend.com/2018/02/07/laravel-ajax-tutorial-example/
- https://www.tutorialspoint.com/laravel/laravel_ajax.htm

- https://stackoverflow.com/questions/21839910/ajax-upload-file-after-choose-file-auto-submit
- https://laracasts.com/discuss/channels/laravel/laravel-56-ajax-call-419-unknown-status

- CSRF Protection
   - https://laravel.com/docs/5.7/csrf
- HTTP Requests   
    - https://laravel.com/docs/5.7/requests

#### auto upload ajax
- https://www.google.com/search?q=auto+upload+ajax&oq=auto+upload+ajax&aqs=chrome..69i57j0l2.18349j0j7&sourceid=chrome&ie=UTF-8
- https://stackoverflow.com/questions/21839910/ajax-upload-file-after-choose-file-auto-submit



### Equaciona matemática
- https://www.youtube.com/channel/UCZLyNRqqp2MeFuwuZdbGDJw/videos

/testuploader/public/assets/js/components/hs.file-attachement.js


- http://nixbox.com/demos/jquery-uploadprogress.php

### Laravel AJAX Tutorial Example
- https://appdividend.com/2018/02/07/laravel-ajax-tutorial-example/  


### CKEditor Ecosystem Documentation
- https://ckeditor.com/docs/ckeditor5/latest/builds/guides/overview.html#classic-editor
- Image Uploads with CKEditor and Laravel
    - https://coderwall.com/p/xs1rsa/image-uploads-with-ckeditor-and-laravel?fbclid=IwAR1JxaOHzMlYWS0fR1n8hlMwi2tyRR0srx6X-HgU3hnwSNI0wNQ2K-THwH0
- How to add ckeditor with image upload in Laravel ?
    - https://itsolutionstuff.com/post/how-to-add-ckeditor-with-image-upload-in-laravel-example.html?fbclid=IwAR2vSqsreyL_syKl7aN0LMQzrt3_fulGovv8I5TR5sWHvxvvpMKfFSUdF3s
- Laravel User Image
    - https://www.youtube.com/watch?v=jy2SUxx6uHc
    - https://www.youtube.com/user/devdojo/playlists
### How it works

The route `Route::view('/grocery', 'grocery');` loads the view `resources/views/grocery.blade.php.`
The view resources/views/grocery.blade.php contains the `<meta name="_token" content="{{csrf_token()}}" />` and it run the 
AJAX jquery with the follow settings:

```
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});
```

When the user send the form the button `<button class="btn btn-primary" id="ajaxSubmit">Submit</button>`
is prevented to behave as default `e.preventDefault();`, then the AJAX call is sent
to the route `url: "{{ url('/grocery/post') }}",` that redirects to the controller
```
public function store(Request $req)
    {
        //return response()->json(['success'=>'Data is successfully addedqqa']);
        return response()->json(['success'=>$req->input('name')]);
    }

``` 

With intercepts the request (Form with input fields) and returns a JSON string
to the JQuery as a Success.



This is the most important code in the page:

```
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    ...
    <meta name="_token" content="{{csrf_token()}}" />
    ...
</head>
<body>

... Body content ...

<script src="http://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
</script>
<script>
    jQuery(document).ready(function(){
        jQuery('#ajaxSubmit').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: "{{ url('/grocery/post') }}",
                method: 'post',
                data: {
                    name: jQuery('#name').val(),
                    type: jQuery('#type').val(),
                    price: jQuery('#price').val()
                },
                success: function(result){
                    jQuery('.alert').show();
                    jQuery('.alert').html(result.success);
                }});
        });
    });
</script>
</body>
</html>


```
